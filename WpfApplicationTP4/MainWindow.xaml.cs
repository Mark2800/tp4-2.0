﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace tp4
{

    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// 
    /// </summary>
    public partial class MainWindow : Window
    {
        private String now = "app";
        private String path = Environment.CurrentDirectory;

        public MainWindow()
        {
            InitializeComponent();
            SetImage("app");
            Nouveautes.SelectedIndex = 0;
            Nouveautes.ScrollIntoView(Nouveautes.Items[0]);
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            SetImage("app");
            now = "app";
        }

        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            SetImage("film");
            now = "film";
        }

        private void MenuItem_Click_2(object sender, RoutedEventArgs e)
        {
            SetImage("musique");
            now = "musique";
        }

        private void ensemble_click(object sender, RoutedEventArgs e)
        {
            if (sender.Equals(Ensemble1))
            {
                if (now.Equals("app"))
                {

                }
                else if (now.Equals("film"))
                {
                    System.Diagnostics.Process.Start("http://www.imdb.com/title/tt2277860/?ref_=nv_sr_2");
                }
                else if (now.Equals("musique"))
                {
                    emettreSon(1);
                }

            }
            else if (sender.Equals(Ensemble2))
            {
                if (now.Equals("app"))
                {

                }
                else if (now.Equals("film"))
                {
                    System.Diagnostics.Process.Start("http://www.imdb.com/title/tt2567026/?ref_=fn_al_tt_4");
                }
                else if (now.Equals("musique"))
                {
                    emettreSon(2);
                }
            }
            else if (sender.Equals(Ensemble3))
            {
                if (now.Equals("app"))
                {

                }
                else if (now.Equals("film"))
                {
                    System.Diagnostics.Process.Start("http://www.imdb.com/title/tt1700841/?ref_=fn_al_tt_3");
                }
                else if (now.Equals("musique"))
                {
                    emettreSon(3);
                }
            }
            else if (sender.Equals(Ensemble4))
            {
                if (now.Equals("app"))
                {

                }
                else if (now.Equals("film"))
                {
                    System.Diagnostics.Process.Start("http://www.imdb.com/title/tt0918940/?ref_=nv_sr_1");
                }
                else if (now.Equals("musique"))
                {
                    emettreSon(4);
                }
            }
        }

        private void emettreSon(int numEnsemble)
        {
            MediaPlayer media = new MediaPlayer();
            media.Open(new Uri(path + @"\Resources\son" + numEnsemble + ".wav"));
            media.Play();
        }

        private void SetImage(string section)
        {
            if (section.Equals("app"))
            {
                ajusterImages(100);
                image1.BeginInit();
                image1.Source = new BitmapImage(new Uri("Resources/app1.png", UriKind.RelativeOrAbsolute));
                image1.EndInit();
                titre1.Content = "App numero 1";
                image2.BeginInit();
                image2.Source = new BitmapImage(new Uri("Resources/app2.png", UriKind.RelativeOrAbsolute));
                image2.EndInit();
                titre2.Content = "App numero 2";
                image3.BeginInit();
                image3.Source = new BitmapImage(new Uri("Resources/app3.png", UriKind.RelativeOrAbsolute));
                image3.EndInit();
                titre3.Content = "App numero 3";
                image4.BeginInit();
                image4.Source = new BitmapImage(new Uri("Resources/app4.png", UriKind.RelativeOrAbsolute));
                image4.EndInit();
                titre4.Content = "Angry Birds 2";
                descr4.Content = "Rovio Entertainment Ldt.";
            }
            else if (section.Equals("musique"))
            {
                ajusterImages(100);
                image1.BeginInit();
                image1.Source = new BitmapImage(new Uri("Resources/musique1.png", UriKind.RelativeOrAbsolute));
                image1.EndInit();
                titre1.Content = "This House Is Not For Sale";
                descr1.Content = "Bon Jovi";
                image2.BeginInit();
                image2.Source = new BitmapImage(new Uri("Resources/musique2.png", UriKind.RelativeOrAbsolute));
                image2.EndInit();
                titre2.Content = "Collage EP";
                descr2.Content = "The Chainsmokers";
                image3.BeginInit();
                image3.Source = new BitmapImage(new Uri("Resources/musique3.png", UriKind.RelativeOrAbsolute));
                image3.EndInit();
                titre3.Content = "The Stage";
                descr3.Content = "Avenged Sevenfold";
                image4.BeginInit();
                image4.Source = new BitmapImage(new Uri("Resources/musique4.png", UriKind.RelativeOrAbsolute));
                image4.EndInit();
                titre4.Content = "Layers";
                descr4.Content = "Kungs";
            }
            else if (section.Equals("film"))
            {
                ajusterImages(150);

                image1.BeginInit();
                image1.Source = new BitmapImage(new Uri("Resources/film1.png", UriKind.RelativeOrAbsolute));
                image1.EndInit();
                titre1.Content = "Le monde de Dory";
                descr1.Content = "Audio en anglais";
                image2.BeginInit();
                image2.Source = new BitmapImage(new Uri("Resources/film2.png", UriKind.RelativeOrAbsolute));
                image2.EndInit();
                titre2.Content = "Alice de l'autre côté du miroir";
                descr2.Content = "Audio en anglais";
                image3.BeginInit();
                image3.Source = new BitmapImage(new Uri("Resources/film3.png", UriKind.RelativeOrAbsolute));
                image3.EndInit();
                titre3.Content = "Sausage Party";
                descr3.Content = "Audio en anglais";
                image4.BeginInit();
                image4.Source = new BitmapImage(new Uri("Resources/film4.png", UriKind.RelativeOrAbsolute));
                image4.EndInit();
                titre4.Content = "Tarzan";
                descr4.Content = "Audio en anglais";
            }

        }

        /**
         * Set la hauteur des images 
         */
        private void ajusterImages(int hauteur)
        {
            image1.Height = hauteur;
            image2.Height = hauteur;
            image3.Height = hauteur;
            image4.Height = hauteur;
        }

        private void right_Click(object sender, RoutedEventArgs e)
        {

            int longeur = Nouveautes.Items.Count;
            int index = Nouveautes.SelectedIndex;
            Console.WriteLine(longeur);
            Console.WriteLine(index);
            if (index == longeur - 1)
            {
                index = -1;
            }
            Nouveautes.SelectedIndex = index + 1;
            Nouveautes.ScrollIntoView(Nouveautes.Items[index + 1]);


        }

        private void left_Click(object sender, RoutedEventArgs e)
        {
            int longeur = Nouveautes.Items.Count;
            int index = Nouveautes.SelectedIndex;
            Console.WriteLine(longeur);
            Console.WriteLine(index);
            if (index == 0)
            {
                index = longeur;
            }
            Nouveautes.SelectedIndex = index - 1;

            Nouveautes.ScrollIntoView(Nouveautes.Items[index - 1]);
        }

    }
}